#!/usr/bin/env python

from distutils.core import setup

setup(name='bettertimes_util',
      version='0.1',
      description='Utility package for usage with BetterTimes to create networked SMC applications',
      author='Per Hallgren',
      author_email='per.zut@gmail.com',
      packages=['bettertimes_util'],
      requires=['bettertimes', 'gmpy2'],
)
