
This library provides networking, benchmarking utilities, and a bit of
configuration file management for use together with the bettertimes library

Dependencies
============

First install `bettertimes` from https://bitbucket.org/hallgrep/bettertimes.
Now you'll need the `twisted` networking engine, which can be installed via
pip:

    pip install twisted

Installing
==========

Simply install using distutils:

    python setup.py install

Usage
=====

See examples under `./bettertimes_util/example`.


