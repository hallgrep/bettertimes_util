import atexit
import time


def timestamped(label=None):
    """ This is a decorator, to annotate any test case that we want to time stamp """

    def decorator(func):
        def wrapper(self, *args, **kwargs):
            if not hasattr(self.__class__, 'stamps'):
                setattr(self.__class__, 'stamps', [])
                atexit.register(lambda: print_stamps(self.stamps))

            t = time.time()
            stamp = []

            def stop():
                stamp.append(time.time())

            self.stop = stop
            output = func(self, *args, **kwargs)

            stamp = stamp[0] if stamp else time.time()

            local_label = label
            if local_label is None:
                local_label = func.__name__
            elif hasattr(self.__class__, 'dynamic_label'):
                local_label = label % self.__class__.dynamic_label

            self.__class__.stamps.append((local_label, stamp - t))
            return output

        wrapper.__name__ = func.__name__

        return wrapper

    return decorator


def print_stamps(stamps):
    w = 80
    print "\n\n%s" % fill("=", w)
    print "== All timestamps\n"

    print_pairs(stamps, w)

    print "\n\n%s" % fill("-", w)
    print "-- Averages\n"

    grouped = {}
    for stamp in stamps:
        label = stamp[0]
        grouped.setdefault(label, []).append(stamp[1])

    sums = []
    max_label = 0
    for label, times in grouped.iteritems():
        max_label = max(max_label, len(label))
        sums.append((label, sum(times) / len(times), len(times)))

    print_pairs(sums, w, max_label=max_label)

    print "\n%s\n\n" % fill("=", w)


def print_pairs(pairs, w, max_label=0):
    sort = sorted(pairs, key=lambda x: x[0])
    template = '%s seconds'
    const = len(template.replace('%%s', ''))

    for item in sort:
        label, timestamp = item[:2]

        time = "%s%s" % (timestamp, " " * (17 - len(str(timestamp))))

        if len(item) == 3:
            invocations = "(%s invocations)" % item[2]
            label = create_padding_between(label, invocations, " ", max_label+20)

        print template % (create_padding_between(label, time, ".", w - const))


def create_padding_between(string1, string2, padding, target_width):
    padding_width = target_width - len(string1) - len(string2) - 2
    dots = padding * ((padding_width) // len(padding))
    dots = " " * (padding_width - len(dots)) + dots  # Add a space if the padding wouldn't fit
    result = "%s %s %s" % (string1, dots, string2)
    return result


def fill(dash, w):
    return dash * (w / len(dash))
