import ConfigParser
import random

from bettertimes_util.crypto import CryptoParams


def create_config():
    config_file = 'config.cfg'

    config = ConfigParser.RawConfigParser()
    config.read(config_file)

    crypto_params = CryptoParams(config)
    crypto_params.generate_keys()

    add_section_safe(config, crypto_params.private_key_label)
    for name, value in crypto_params.private_key_items():
        config.set(crypto_params.private_key_label, name, value)

    add_section_safe(config, crypto_params.public_key_label)

    for name, value in crypto_params.public_key_items():
        config.set(crypto_params.public_key_label, name, value)

    add_section_safe(config, 'Network')
    config.set('Network', 'port', random.randint(10000, 32000))
    config.set('Network', 'address', 'localhost')

    with open(config_file, 'wb') as configfile:
        config.write(configfile)


def add_section_safe(config, label):
    (label in config.sections()) or config.add_section(label)


if __name__ == '__main__':
    create_config()
