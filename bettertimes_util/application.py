from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol
from twisted.internet import reactor, protocol, endpoints
import ConfigParser


def run_client(client):
    config = ConfigParser.ConfigParser()
    config.read('config.cfg')
    port = int(config.get('Network', 'port'))
    address = config.get('Network', 'address')
    endpoint = TCP4ClientEndpoint(reactor, address, port)
    connectProtocol(endpoint, client)
    reactor.run()


def create_server(server_factory):
    # Each client talks to a different instance of the server protocol class.
    # Therefore, we here pass a factory instead of an instance
    class ServerHandlerFactory(protocol.Factory):
        def buildProtocol(self, addr):
            return server_factory()

    config = ConfigParser.ConfigParser()
    config.read('config.cfg')

    port = int(config.get('Network', 'port'))

    print "Starting server"
    endpoints.serverFromString(reactor, "tcp:%s" % port).listen(ServerHandlerFactory())
    reactor.run()