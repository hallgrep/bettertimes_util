from bettertimes_util.application import create_server
from bettertimes_util.stateful_line_protocol import StatefulLineProtocol, next_state, final_state


class RepeatedServer(StatefulLineProtocol):
    """
    This protocol is much like the toy protocol, but we want to compute 17*(2^10) instead.
    """

    @next_state('multiply')
    def first_state(self):
        return None

    @next_state('multiply_final', repeat=9)
    def multiply(self, double_me=None):
        return {"doubled": double_me * 2}

    @final_state
    def multiply_final(self, double_me=None):
        return {"doubled": double_me * 2}


if __name__ == '__main__':
    create_server(lambda: RepeatedServer())
