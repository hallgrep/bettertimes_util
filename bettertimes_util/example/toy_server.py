from bettertimes_util.application import create_server
from bettertimes_util.stateful_line_protocol import StatefulLineProtocol, next_state, final_state

from bettertimes_util.timestamps import timestamped


class ToyServer(StatefulLineProtocol):
    """
    The idea of this protocol is just for the client to send the number 17 to
    the server, which multiplies it by 2 and sends the result (34) back to the
    client
    """

    # This is just a stub state needed in the server. It is a design issue,
    # TODO: remove this
    @next_state('multiply')
    def first_state(self):
        return None

    # Label for timestamp
    @timestamped('toy_response')
    # There's only one state at the server for this example
    @final_state
    # First state must be called exactly 'first_state'
    def multiply(self, double_me=None):
        return {"doubled": double_me * 2}


if __name__ == '__main__':
    create_server(lambda: ToyServer())
