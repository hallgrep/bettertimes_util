import ConfigParser
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol

from bettertimes_util.stateful_line_protocol import StatefulLineProtocol, next_state, final_state


class DynamicClient(StatefulLineProtocol):
    """
    This protocol is much like the toy protocol, but we want to compute 17*(2^10) instead.
    """

    def __init__(self):
        StatefulLineProtocol.__init__(self, is_client=True, keep_reactor=True)

    @next_state('repeated_doubling')
    def first_state(self):
        return {"double_me": 17}

    @next_state('parse_result', repeat=9)
    def repeated_doubling(self, doubled):
        return {"double_me": doubled}

    @final_state
    def parse_result(self, doubled=None):
        print("The doubled secret is %s" % doubled)
        return doubled


if __name__ == '__main__':
    config = ConfigParser.ConfigParser()
    config.read('config.cfg')
    port = int(config.get('Network', 'port'))
    address = config.get('Network', 'address')
    endpoint = TCP4ClientEndpoint(reactor, address, port)

    for i in range(5):
        connectProtocol(endpoint, DynamicClient())

    reactor.run()
