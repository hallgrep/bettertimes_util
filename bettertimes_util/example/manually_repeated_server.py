from bettertimes_util.application import create_server
from bettertimes_util.stateful_line_protocol import StatefulLineProtocol, next_state, final_state


class RepeatedServer(StatefulLineProtocol):
    """
    This protocol is much like the toy protocol, but we want to compute 17*(2^10) instead.
    """

    @next_state('multiply1')
    def first_state(self):
        return None

    @next_state('multiply2')
    def multiply1(self, double_me=None):
        return {"doubled": double_me * 2}

    @next_state('multiply3')
    def multiply2(self, double_me=None):
        return {"doubled": double_me * 2}

    @next_state('multiply4')
    def multiply3(self, double_me=None):
        return {"doubled": double_me * 2}

    @next_state('multiply5')
    def multiply4(self, double_me=None):
        return {"doubled": double_me * 2}

    @next_state('multiply6')
    def multiply5(self, double_me=None):
        return {"doubled": double_me * 2}

    @next_state('multiply7')
    def multiply6(self, double_me=None):
        return {"doubled": double_me * 2}

    @next_state('multiply8')
    def multiply7(self, double_me=None):
        return {"doubled": double_me * 2}

    @next_state('multiply9')
    def multiply8(self, double_me=None):
        return {"doubled": double_me * 2}

    @next_state('multiply_final')
    def multiply9(self, double_me=None):
        return {"doubled": double_me * 2}

    @final_state
    def multiply_final(self, double_me=None):
        return {"doubled": double_me * 2}


if __name__ == '__main__':
    create_server(lambda: RepeatedServer())
