from bettertimes_util.application import run_client

from bettertimes_util.stateful_line_protocol import StatefulLineProtocol, next_state, final_state
from bettertimes_util.timestamps import timestamped


class ToyClient(StatefulLineProtocol):
    """
    The idea of this protocol is just for the client to send the number 17 to
    the server, which multiplies it by 2 and sends the result (34) back to the
    client
    """

    def __init__(self):
        StatefulLineProtocol.__init__(self, True)

    # Label for timestamp
    @timestamped('create_toy_request')
    # Name of next function
    @next_state('parse_result')
    # First state must be called exactly 'first_state'
    def first_state(self):
        self.number = 17

        # Triggers 'multiply' state at server
        return {"double_me": self.number}

    @timestamped()  # If no label is supplied, timestamps use the function name
    @final_state
    def parse_result(self, doubled=None):
        print("The doubled secret is %s" % doubled)
        return doubled


if __name__ == '__main__':
    run_client(ToyClient())
