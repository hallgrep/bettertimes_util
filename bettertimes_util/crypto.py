import argparse
import pickle
import time

from bettertimes.crypto.keypair import KeyPair
from bettertimes.crypto.schemes import dgk, paillier
from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.paillier import Paillier


def create_argument_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument('--scheme', type=str, default="Paillier")
    parser.add_argument('--keysize', type=int, default=2048)
    parser.add_argument('--dgk_l', type=int, default=2)
    parser.add_argument('--allow_decryption', type=bool, default=True)

    return parser


class CryptoParams(object):
    def __init__(self, config, args=None):
        super(CryptoParams, self).__init__()
        self.config = config
        if not args:
            args = create_argument_parser().parse_args()

        self.args = args
        self.scheme = args.scheme
        self.key_size = args.keysize
        self.allow_decryption = args.allow_decryption

    def load_private_key(self):
        if self.scheme == 'Paillier':
            l = int(self.private_key('l'))
            m = int(self.private_key('m'))
            p = int(self.private_key('p'))
            q = int(self.private_key('q'))

            sk = paillier.PrivateKey(l=l, m=m, p=p, q=q)
        else:  # if self.scheme == 'DGK':
            vp = int(self.private_key('vp'))
            vq = int(self.private_key('vq'))
            p = int(self.private_key('p'))
            q = int(self.private_key('q'))
            decryption_table = pickle.loads(self.private_key('decryption_table'))

            sk = dgk.PrivateKey(vp=vp, vq=vq, p=p, q=q, decryption_table=decryption_table)

        return sk

    def load_public_key(self):
        if self.scheme == 'Paillier':
            n = int(self.public_key('n'))
            g = int(self.public_key('g'))
            bits = int(self.public_key('bits'))

            pk = paillier.PublicKey(n=n, g=g, n_sq=n * n, bits=bits)
        else:  # if self.scheme == 'DGK':
            n = int(self.public_key('n'))
            g = int(self.public_key('g'))
            h = int(self.public_key('h'))
            u = int(self.public_key('u'))
            l = int(self.public_key('l'))
            bits = int(self.public_key('bits'))

            pk = dgk.PublicKey(n=n, g=g, h=h, u=u, l=l, bits=bits)

        return pk

    def load_scheme(self):
        if self.scheme == 'Paillier':
            return Paillier()
        elif self.scheme == 'DGK':
            return DGK()

    def private_key(self, prop):
        return self.config.get(self.private_key_label, prop)

    def public_key(self, prop):
        return self.config.get(self.public_key_label, prop)

    @property
    def private_key_label(self):
        return '%s.PrivateKey' % self.scheme

    @property
    def public_key_label(self):
        return '%s.PublicKey' % self.scheme

    def private_key_items(self):
        if self.scheme == 'Paillier':
            return [('l', getattr(self.key_pair, 'l')),
                    ('m', getattr(self.key_pair, 'm')),
                    ('p', getattr(self.key_pair, 'p')),
                    ('q', getattr(self.key_pair, 'q'))]

        elif self.scheme == 'DGK':
            return [('vp', getattr(self.key_pair, 'vp')),
                    ('vq', getattr(self.key_pair, 'vq')),
                    ('p', getattr(self.key_pair, 'p')),
                    ('q', getattr(self.key_pair, 'q')),
                    #('decryption_table', getattr(self.key_pair, 'decryption_table'))
                    ('decryption_table', pickle.dumps(getattr(self.key_pair, 'decryption_table')))]

    def public_key_items(self):
        if self.scheme == 'Paillier':
            return [('n', getattr(self.key_pair, 'n')),
                    ('g', getattr(self.key_pair, 'g')),
                    ('bits', getattr(self.key_pair, 'bits'))]

        elif self.scheme == 'DGK':
            return [('n', getattr(self.key_pair, 'n')),
                    ('g', getattr(self.key_pair, 'g')),
                    ('h', getattr(self.key_pair, 'h')),
                    ('u', getattr(self.key_pair, 'u')),
                    ('l', getattr(self.key_pair, 'l')),
                    ('bits', getattr(self.key_pair, 'bits'))]

    def generate_keys(self):
        if self.scheme == 'Paillier':
            paillier = Paillier()
            self.key_pair = paillier.keygen(self.key_size)
        elif self.scheme == 'DGK':
            dgk = DGK()
            self.key_pair = dgk.keygen(self.key_size, allow_decrypt=True, l=self.args.dgk_l)

    def load_key_pair(self):
        key_pair = KeyPair(self.load_public_key(), self.load_private_key())

        if self.scheme == 'DGK' and self.allow_decryption and not key_pair.private_key.decryption_table:
            print "Starting to compute decryption table at t=%s" % time.time()
            self.load_scheme().add_decryption_table(key_pair)
            print "Finished computing decryption table at t=%s" % time.time()

        return key_pair
