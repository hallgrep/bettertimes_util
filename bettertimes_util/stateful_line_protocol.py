import pickle
from twisted.internet import reactor
from twisted.internet.protocol import connectionDone, Protocol
from twisted.protocols import basic


def loads(inp):
    return pickle.loads(inp)


def dumps(inp):
    return pickle.dumps(inp)


def next_state(next_state_name, repeat=None):
    def next_state_decorator(func):
        def wrapper(self, *argv):
            if not hasattr(self, '__slp_closure'):
                setattr(self, '__slp_closure', {'num': 1})

            if argv:
                params = loads(*argv)
                output = func(self, **params)
            else:
                output = func(self)

            if repeat and repeat > self.__slp_closure['num']:
                self.__slp_closure['num'] += 1

                next_method = getattr(self, func.__name__)
                return ProtocolState(next_method, output)
            else:
                if repeat:
                    # Reset counter after completed repetition
                    self.__slp_closure['num'] = 1

                next_method = getattr(self, next_state_name)
                return ProtocolState(next_method, output)

        wrapper.__name__ = func.__name__
        return wrapper

    return next_state_decorator


def final_state(func):
    def wrapper(self, param):
        params = loads(param)
        result = func(self, **params)
        pickled = result
        return ProtocolState(None, pickled)

    wrapper.__name__ = func.__name__
    return wrapper


class ProtocolState(object):
    def __init__(self, next, output=None):
        super(ProtocolState, self).__init__()

        self.next = next
        self.output = output

    def __str__(self):
        return "ProtocolState{next=%s}" % (self.next.__name__ if self.next else None)


class StatefulLineProtocol(basic.LineReceiver):
    MAX_LENGTH = 2 ** 24  # 16 MB

    def rawDataReceived(self, data):
        pass

    def __init__(self, is_client=False, keep_reactor=False):
        self.state = None
        self.is_client = is_client
        self.keep_reactor = keep_reactor

    def makeConnection(self, transport):
        basic.LineReceiver.makeConnection(self, transport)
        if self.is_client:
            state = self.first_state()
            # Initiate protocol
            print("Initiating protocol as client")
            self.send_line_serialized(state.output)
            self.state = state
        else:
            print("Initiating protocol as server")
            self.state = self.first_state()
            print("Initiated")

    def connectionLost(self, reason=connectionDone):
        Protocol.connectionLost(self, reason)

        # if self.state.next:
        # print("Running with empty input to shut down.")
        # final_state = self.call_next("{}")
        # print("Got: %s" % (final_state.output if final_state else final_state))

        print("Connection closed")
        if self.is_client and not self.keep_reactor:
            reactor.callFromThread(reactor.stop)

        self.call_next(dumps({}))

    def first_state(self):
        """
        :rtype: ProtocolState
        """
        raise NotImplementedError

    def call_next(self, line):
        upcomming_state = self.state.next
        if upcomming_state is None:
            return None

        print("Stepping forward: %s" % self.state)
        return upcomming_state(line)

    def send_line_serialized(self, line):
        serialized_output = dumps(line)
        self.sendLine(serialized_output)

    def lineReceived(self, line):
        if self.state.next:
            state_data = self.call_next(line)

            if state_data:
                self.send_line_serialized(state_data.output)
                self.state = state_data
            else:
                print("Final state. Closing connection.")
                self.transport.loseConnection()
        else:
            print("Closing connection after final response.")
            self.transport.loseConnection()

    def lineLengthExceeded(self, line):
        print("Line length exceeded 16 MB, and wasn't handled. You can try to increase this by setting self.MAX_LENGTH "
              "to a greater value than 2**24 in your protocol implementation")
        return basic.LineReceiver.lineLengthExceeded(self, line)
